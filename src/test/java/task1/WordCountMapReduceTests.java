package task1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;
import task1.mappers.WordMapper;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class WordCountMapReduceTests {
    MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        WordMapper mapper = new WordMapper();
        MaxLengthReducer reducer = new MaxLengthReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "asd dd a"));
        mapReduceDriver.withOutput(new Text("asd"), new IntWritable(3));
        mapReduceDriver.runTest();
    }
}
