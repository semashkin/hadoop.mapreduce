package task1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;
import task1.mappers.WordMapperWithOptimization;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 23.07.2016.
 */
public class WordMapperWithOptimizationTests {
    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

    @Before
    public void setUp() {
        WordMapperWithOptimization mapper = new WordMapperWithOptimization();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(), new Text(
                "abc b"));
        mapDriver.withOutput(new Text("abc"), new IntWritable(3));
        mapDriver.runTest();
    }
}