package task3.sequence.output;

import task3.model.IpWritableModel;
import task3.reducers.IpReducer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpReducerTests  {
    ReduceDriver<Text, LongWritable, Text, IpWritableModel> reduceDriver;

    @Before
    public void setUp() {
        IpReducer reducer = new IpReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer() throws IOException {
        List<LongWritable> values = new ArrayList<LongWritable>();
        values.add(new LongWritable(1));
        values.add(new LongWritable(2));
        values.add(new LongWritable(3));
        reduceDriver.withInput(new Text("ip1"), values);
        reduceDriver.withOutput(new Text("ip1"), new IpWritableModel(2,6));
        reduceDriver.runTest();
    }
}
