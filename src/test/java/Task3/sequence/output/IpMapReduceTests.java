package task3.sequence.output;

import task3.mappers.IpMapper;
import task3.model.IpWritableModel;
import task3.reducers.IpReducer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpMapReduceTests {
    MapReduceDriver<LongWritable, Text, Text, LongWritable, Text, IpWritableModel> mapReduceDriver;

    @Before
    public void setUp() {
        IpMapper mapper = new IpMapper();
        IpReducer reducer = new IpReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new IpWritableModel(40028,40028));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessRowWhenThereIsNoBites() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 ThereIsNoBites \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 123 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new IpWritableModel(123,123));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessRowWhenThereIsNoIp() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ThereIsNoI_P12 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 123 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new IpWritableModel(123,123));
        mapReduceDriver.runTest();
    }
}
