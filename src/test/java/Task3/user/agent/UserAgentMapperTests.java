package task3.user.agent;

import task3.mappers.UserAgentMapper;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentMapperTests {
    MapDriver<LongWritable, Text, Text, Text> mapDriver;

    @Before
    public void setUp() {
        UserAgentMapper mapper = new UserAgentMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void should_process_row() throws IOException {
        mapDriver.withInput(new LongWritable(1), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapDriver.withOutput(new Text("Mozilla"), new Text("ip1"));
        mapDriver.runTest();
    }
}