package task3.user.agent;

import task3.reducers.UserAgentReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentReducerTests {
    ReduceDriver<Text, Text, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        UserAgentReducer reducer = new UserAgentReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer() throws IOException {
        List<Text> values = new ArrayList<Text>();
        values.add(new Text("ip1"));
        values.add(new Text("ip1"));
        values.add(new Text("ip1"));
        reduceDriver.withInput(new Text("Ololoshka"), values);
        // 1 - cos we count only unique users
        reduceDriver.withOutput(new Text("Ololoshka"), new IntWritable(1));
        reduceDriver.runTest();
    }

    @Test
    public void shouldCountWhenValuesIsUnique() throws IOException {
        List<Text> values = new ArrayList<Text>();
        values.add(new Text("ip1"));
        values.add(new Text("ip3"));
        values.add(new Text("ip123123"));
        values.add(new Text("ip123123"));
        reduceDriver.withInput(new Text("Ololoshka"), values);
        // 3 - cos we count only unique users
        reduceDriver.withOutput(new Text("Ololoshka"), new IntWritable(3));
        reduceDriver.runTest();
    }
}
