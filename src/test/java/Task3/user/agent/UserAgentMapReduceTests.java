package task3.user.agent;

import task3.mappers.UserAgentMapper;
import task3.reducers.UserAgentReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentMapReduceTests {
    MapReduceDriver<LongWritable, Text, Text, Text, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        UserAgentMapper mapper = new UserAgentMapper();
        UserAgentReducer reducer = new UserAgentReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void shouldProcessRow() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("Mozilla"), new IntWritable(1));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessWhenIpIsNotUnique() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("Mozilla"), new IntWritable(1));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessWhenIpIsUnique() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip2 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("Mozilla"), new IntWritable(2));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessDifferentuserAgents() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Ololoshka/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("Mozilla"), new IntWritable(1));
        mapReduceDriver.withOutput(new Text("Ololoshka"), new IntWritable(1));
        mapReduceDriver.runTest();
    }
}
