package task3.nice.csv;

import task3.reducers.IpReducerWithNiceCsv;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpReducerWithNiceCsvTests {
    ReduceDriver<Text, LongWritable, Text, Text> reduceDriver;

    @Before
    public void setUp() {
        IpReducerWithNiceCsv reducer = new IpReducerWithNiceCsv();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer() throws IOException {
        List<LongWritable> values = new ArrayList<LongWritable>();
        values.add(new LongWritable(1));
        values.add(new LongWritable(2));
        values.add(new LongWritable(3));
        reduceDriver.withInput(new Text("ip1"), values);
        reduceDriver.withOutput(new Text("ip1"), new Text("2,6"));
        reduceDriver.runTest();
    }
}