package task3;

import task3.services.IpInfoService;
import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpInfoServiceTests  {
    private IpInfoService sut;

    @Before
    public void setUp() {
        sut=new IpInfoService();
    }

    @Test
    public void should_return_ip() throws IOException {
        String ip = sut.getIp(new Text("ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        assert(ip).equals("ip1");
    }

    @Test
    public void should_return_bites() throws IOException {
        Long bites = sut.getBites(new Text("ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        assert(bites).equals(new Long(bites));
    }

    @Test
    public void shouldReturnuserAgent() throws IOException {
        String bites = sut.getUserAgent(new Text("ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        assert(bites).equals("Mozilla");
    }

    @Test
    public void shouldReturnuserAgentJava() throws IOException {
        String bites = sut.getUserAgent(new Text("ip43 - - [24/Apr/2011:06:37:11 -0400] \"GET /apache/ HTTP/1.1\" 200 1945 \"-\" \"Java/1.6.0_04\""));
        assert(bites).equals("Java");
    }

    @Test
    public void shouldReturnuserAgentWithLink() throws IOException {
        String bites = sut.getUserAgent(new Text("ip48 - - [24/Apr/2011:06:50:50 -0400] \"GET /sgi_indigo2/purple.jpg HTTP/1.1\" 200 52356 \"http://www.google.com.tw/imgres?imgurl=http://host2/sgi_indigo2/purple.jpg&imgrefurl=http://host2/sgi_indigo2/&h=380&w=591&sz=52&tbnid=vxrN0QWR3xCpSM:&tbnh=87&tbnw=135&prev=/search%3Fq%3DSGI%2Bindigo2%26tbm%3Disch%26tbo%3Du&zoom=1&q=SGI+indigo2&hl=zh-TW&usg=__pGgsYwM038cojgXX4aZ1MDcIYg0=&sa=X&ei=SNWzTYvRJ4KmugP4hbmNBw&ved=0CDwQ9QEwAg\" \"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E)\""));
        assert(bites).equals("Mozilla");
    }

    @Test
    public void shouldReturnuserAgentNokia5220XpressMusic() throws IOException {
        String bites = sut.getUserAgent(new Text("ip1249 - - [26/Apr/2011:23:06:52 -0400] \"GET / HTTP/1.1\" 200 12550 \"-\" \"Nokia5220XpressMusic/2.0 (06.51) Profile/MIDP-2.1 Configuration/CLDC-1.1\""));
        assert(bites).equals("Nokia5220XpressMusic");
    }
}