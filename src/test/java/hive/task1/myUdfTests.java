package hive.task1;


import hive.task3.myUdf;
import org.apache.hadoop.io.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Semashkin on 10.08.2016.
 */
public class myUdfTests {

    @Test
    public void myUdfUnction() {
        String userAgentString = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17 SE 2.X MetaSr";
        ArrayList<Text> result = (new myUdf()).evaluate(userAgentString);
        assert(result.get(0).equals(new Text("Chrome 24")));
    }
}
