package task2;

import task2.reducers.BidReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class BidReducerTests  {
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        BidReducer reducer = new BidReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        reduceDriver.withInput(new Text("b7120570f1868f55505ba61a1b943ab2"), values);
        reduceDriver.withOutput(new Text("b7120570f1868f55505ba61a1b943ab2"), new IntWritable(2));
        reduceDriver.runTest();
    }
}
