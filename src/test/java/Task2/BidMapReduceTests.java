package task2;

import task2.mappers.BidMapper;
import task2.reducers.BidReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class BidMapReduceTests  {
    MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        BidMapper mapper = new BidMapper();
        BidReducer reducer = new BidReducer();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "35d93a1b1283c32d6647902c9ddad07c\t20130608000103005\tVhp6Ziu8D8pNkBb\tMozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; KB974489; Media Center PC 6.0; .NET4.0E; .NET4.0C),gzip(gfe),gzip(gfe)\t183.12.188.*\t216\t219\t2\ttrqRTuMvjTN7X9KbuKz\te4d0576e45260289468e8b239c83520a\t\t1493268197\t728\t90\t0\t0\t148\t4b724cd63dfb905ebcd54e64572c646d\t238\t3427\tnull"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "35d93a1b1283c32d6647902c9ddad07c\t20130608000103005\tVhp6Ziu8D8pNkBb\tMozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; KB974489; Media Center PC 6.0; .NET4.0E; .NET4.0C),gzip(gfe),gzip(gfe)\t183.12.188.*\t216\t219\t2\ttrqRTuMvjTN7X9KbuKz\te4d0576e45260289468e8b239c83520a\t\t1493268197\t728\t90\t0\t0\t148\t4b724cd63dfb905ebcd54e64572c646d\t238\t3427\tnull"));
        mapReduceDriver.withOutput(new Text("Vhp6Ziu8D8pNkBb"), new IntWritable(2));
        mapReduceDriver.runTest();
    }
}
