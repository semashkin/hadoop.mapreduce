package common;

import org.apache.hadoop.conf.Configuration;

/**
 * Created by Semashkin Vladimir on 18.07.2016.
 */
public class ConfigurationProvider {
    private Configuration configuration = new Configuration();
    public Configuration get(){
        return configuration;
    }

    public void setProperty(String name,String value){
        configuration.set(name, value);
    }

    public void setOutputSeparator(String outputSeparator) {
        setProperty("mapreduce.output.textoutputformat.separator",outputSeparator);
    }
}
