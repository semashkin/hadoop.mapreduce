package task4.mappers;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import task4.models.ImpKeyWritable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by Semashkin Vladimir on 21.07.2016.
 */
public class ImpMapperWithMapSideJoin extends Mapper<LongWritable, Text, ImpKeyWritable, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private ImpKeyWritable currentKey = new ImpKeyWritable();
    private HashMap<Integer, String> cities = new HashMap<Integer, String>();

    private void readCityNames(URI uri,Context context) throws IOException{
        FSDataInputStream dataIn = FileSystem.get(context.getConfiguration()).open(new Path(uri));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataIn));
        String line = bufferedReader.readLine();
        while (line != null) {
            StringTokenizer tokenizer = new StringTokenizer(line);
            int key = Integer.parseInt(tokenizer.nextToken());
            String cityName = tokenizer.nextToken();
            cities.put(key, cityName);
            line= bufferedReader.readLine();
        }
    }

    public void setup(Context context) throws IOException{
        URI[] uris = context.getCacheFiles();
        readCityNames(uris[0], context);
    }

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null){
            String[] currentValues = getValues(value);
            String cityName = cities.get(cityId(currentValues));
            String userAgent = getUserAgent(currentValues);
            if (cityName != null && userAgent != null){
                currentKey.set(cityName,userAgent);
                context.write(currentKey, one);
            }
        }
    }

    private String[] getValues(Text value){
        String line = value.toString();
        String[] parts = line.split("\t");
        return  parts;
    }

    private String getUserAgent(String[] values) {
        if (values.length >= 4){
            return values[4];
        }

        return null;
    }

    private Integer cityId(String[] values){
        if (values.length >= 7){
            String strCityId = values[7];
            return Integer.parseInt(strCityId);
        }

        return 0;
    }
}
