package task3.reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentReducer extends Reducer<Text, Text, Text, IntWritable> {
    private IntWritable result = new IntWritable(0);

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {

        ArrayList<String> strValues = new ArrayList<String>();
        for (Text val : values)
        {
            strValues.add(val.toString());
        }
        // Let's calculate unique numbers of users
        HashSet<String> uniqueValues = new HashSet<String>(strValues);
        result.set(uniqueValues.size());
        context.write(key, result);
    }
}

