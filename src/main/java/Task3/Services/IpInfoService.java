package task3.services;

import org.apache.hadoop.io.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpInfoService {
    public String getIp(Text input){
        Pattern p = Pattern.compile("ip[0-9]{1,10}");
        Matcher m = p.matcher(input.toString());
        if (m.find()){
            return m.group(0);
        }
        return null;
    }

    public Long getBites(Text input) {
        Pattern p = Pattern.compile("(?<=200 )[0-9]{1,10}");
        Matcher m = p.matcher(input.toString());
        if (m.find()){
            return Long.parseLong(m.group(0));
        }
        return null;
    }

    public String getUserAgent(Text input) {
        Pattern p = Pattern.compile("(?<=\" \")[A-Za-z0-9]+");
        Matcher m = p.matcher(input.toString());
        if (m.find()){
            return m.group(0);
        }
        return null;
    }
}
