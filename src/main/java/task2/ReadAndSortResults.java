package task2;


import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

/**
 * Created by Semashkin Vladimir on 24.07.2016.
 */
public class ReadAndSortResults {

    public static void main(String[] args) throws Exception {
        Path pt=new Path(args[0]);
        Configuration conf = new Configuration();
        conf.set("mapred.child.java.opts","-Xmx4G");
        FileSystem fs = FileSystem.get(conf);
        BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(pt)));
        String line;
        line=br.readLine();
        List< Item > list = new ArrayList< Item >( );
        while (line != null){
            String[] parts = line.split("\t");
            list.add(new Item(line,Integer.parseInt(parts[1])));
            line=br.readLine();
        }

        Collections.sort( list, new ItemComparator() );

        Path file = new Path(args[1]);
        if ( fs.exists( file )) { fs.delete( file, true ); }
        OutputStream os = fs.create( file,
                new Progressable() {
                    public void progress() {
                    } });
        BufferedWriter writeBuffer = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ) );
        try {
            int u = 0;
            for (Item i : list){
                writeBuffer.append(i.getStr());
                System.out.println(i.getStr());
                u++;
                if (u>=100){
                    break;
                }
            }
            writeBuffer.flush();
        }
        finally {
            writeBuffer.close();
            fs.close();
        }
    }

    private static class Item{
        private Integer cnt;
        private String str;
        public Item(String str, Integer cnt){
            this.str = str;
            this.cnt = cnt;
        }

        public String getStr(){
            return this.str;
        }
    }

    private static class ItemComparator implements Comparator<Item> {
        public int compare(Item o1, Item o2) {
            // reverse order
            return o2.cnt.compareTo(o1.cnt);
        }
    }
}
