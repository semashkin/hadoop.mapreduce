package hive.task3;


import eu.bitwalker.useragentutils.UserAgent;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

import java.util.ArrayList;
/**
 * Created by Semashkin on 10.08.2016.
 */
public final class myUdf extends UDF {

    public ArrayList<Text> evaluate(String userAgentString){
        ArrayList<Text> values = new  ArrayList<Text>();
        UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
        values.add(0,new Text(userAgent.getBrowser().getName()));
        values.add(1,new Text(userAgent.getBrowser().getBrowserType().getName()));
        values.add(2,new Text(userAgent.getBrowserVersion().getVersion()));
        values.add(3,new Text(userAgent.getOperatingSystem().getName()));
        values.add(4,new Text(userAgent.getOperatingSystem().getDeviceType().getName()));
        return values;
    }
}
