package task2.mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Semashkin Vladimir on 17.07.2016.
 */
public class BidMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private Text ip = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null){
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            ArrayList<String> currentRow = new ArrayList<String>();
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                currentRow.add(token);
            }
            ip.set(currentRow.get(2));
            context.write(ip, one);
        }
    }
}
