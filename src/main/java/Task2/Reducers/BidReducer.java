package task2.reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 17.07.2016.
 */
public class BidReducer  extends Reducer<Text, IntWritable, Text, IntWritable> {
    private final static IntWritable result = new IntWritable(0);

    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        Integer sum = 0;
        for (IntWritable val : values) {
            sum+=val.get();
        }
        result.set(sum);
        context.write(key, result);
    }


    /*@Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
            *//*super.cleanup(context);*//*
    }*/
}