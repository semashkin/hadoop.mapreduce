package task2;

import common.ConfigurationProvider;
import task2.mappers.BidMapper;
import task2.reducers.BidReducer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by Semashkin Vladimir on 17.07.2016.
 */
public class BidAnalyzer extends Configured implements Tool {
    public static ConfigurationProvider configurationProvider = new ConfigurationProvider();

    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf()); // new approach
        // to take on fly properties and configurations
        job.setJobName("bid job");
        /*
        To distribute code across nodes (not main class only)
         */
        job.setJarByClass(BidAnalyzer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        job.setMapperClass(BidMapper.class);
        job.setReducerClass(BidReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        configurationProvider.setProperty("dfs.datanode.socket.write.timeout", "120000");
        configurationProvider.setProperty("mapreduce.task.timeout", "1200000");
        int res = ToolRunner.run(configurationProvider.get(), new BidAnalyzer(), args);
        System.exit(res);
    }

}
