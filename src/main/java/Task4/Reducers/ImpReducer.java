package task4.reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import task4.models.ImpKeyWritable;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 21.07.2016.
 */
public class ImpReducer  extends Reducer<ImpKeyWritable, IntWritable, Text, IntWritable> {
    private final static IntWritable result = new IntWritable(0);
    private final static int THRESHOLD = 250;

    @Override
    public void reduce(ImpKeyWritable key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        Integer sum = 0;
        for (IntWritable val : values) {
            sum+=val.get();
        }
        if (sum> THRESHOLD){
            result.set(sum);
            context.write(key.getCity(), result);
        }
    }


    /*@Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
            *//*super.cleanup(context);*//*
    }*/
}