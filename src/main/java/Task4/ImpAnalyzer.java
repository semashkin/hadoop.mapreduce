package task4;

import common.ConfigurationProvider;
import task4.mappers.ImpMapperWithMapSideJoin;
import task4.models.ImpKeyWritable;
import task4.partitioners.ImpOperSystemTypePartitioner;
import task4.reducers.ImpReducer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URI;

/**
 * Created by Semashkin Vladimir on 21.07.2016.
 */
public class ImpAnalyzer extends Configured implements Tool {
    public static ConfigurationProvider configurationProvider = new ConfigurationProvider();

    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf()); // new approach
        // to take on fly properties and configurations
        job.setJobName("imp job");
        job.setJarByClass(ImpAnalyzer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setMapOutputKeyClass(ImpKeyWritable.class);
        job.setMapOutputValueClass(IntWritable.class);


        job.setMapperClass(ImpMapperWithMapSideJoin.class);
        job.setPartitionerClass(ImpOperSystemTypePartitioner.class);
        job.setReducerClass(ImpReducer.class);
        job.setNumReduceTasks(4);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.addCacheFile(new URI(args[2]));
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        configurationProvider.setProperty("mapreduce.task.timeout", "300000");
        int res = ToolRunner.run(configurationProvider.get(), new ImpAnalyzer(), args);
        System.exit(res);
    }


}