package task4.partitioners;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;
import task4.models.ImpKeyWritable;

/**
 * Created by Semashkin Vladimir on 21.07.2016.
 */
public class ImpOperSystemTypePartitioner extends Partitioner<ImpKeyWritable, IntWritable>{
    @Override
    public int getPartition(ImpKeyWritable key, IntWritable value, int numReduceTasks) {
        if (key.getUserAgent().toString().contains("Windows NT 5.1")) {
            return 0;
        } else if (key.getUserAgent().toString().contains("Windows NT 6.1")) {
            return 1;
        } else if (key.getUserAgent().toString().contains("AppleWebKit/537.1")) {
            return 2;
        } else {
            return 3;
        }
    }
}
